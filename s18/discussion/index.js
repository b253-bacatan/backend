// console.log("Hello World");


//Functions
	
	//Parameters and Arguments

		// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
		// Functions are mostly created to create complicated tasks to run several lines of code in succession
		// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

		//We also learned in the previous session that we can gather data from user input using a prompt() window.



function printInput(){
	let nickname = prompt ("Enter your nickname:");
	console.log("Hi, " + nickname);
}

// printInput();

function printName(name) {
	console.log("My name is " + name);
}

printName("Clent Bacatan");
printName("Enma Ai");
printName("Kudos");


// Variables can also be passed as an argument
let sampleName = "Yui";

printName(sampleName);

function divisibility(num) {
	let remainder = num % 8;
	console.log("The remainder of" + num + "divided by 8 is:" + remainder);
	
	let isDivisibleBy8 = remainder === 0;
	console.log("Is" + num + " divisible by 8?");
	console.log(isDivisibleBy8);

}

divisibility(64)
divisibility(28)



function argumentFunction(){
	console.log("This function was passed as an argument before the message is printed.");

}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);


// Multiple Parameters

function createFullName(givenName, middleName, surName) {

	console.log(givenName + " " + middleName + " " + surName);

}

createFullName("Clent", "Ramirez", "Bacatan");
createFullName("Clent", "Ramirez", "Bacatan"); // IF ARGUMENT IS LESSER THAN THE PARAMETERS, THE LAST ARGUMENT THAT IS NOT INCLUDED WILL RETURN AS UNDEFINE
createFullName("C", "v", "b", "HI"); // IF ARGUMENT IS MORE THAN THE PARAMETERS, THE EXCEEDING ARGUMENT WILL NOT DISPLAY


let firstName = "Clent";
let middleName = "Ram";
let lastName = "Bacatan";

createFullName(firstName, middleName, lastName);

//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.

//THE RETURN STATEMENT
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName;

	console.log("Cute ko");
	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
}

let completeName = returnFullName("Lucy", " ", "pEve");
console.log(completeName);

console.log(returnFullName(firstName, middleName, lastName));



function returnAddress(city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let address = returnAddress("Cebu", "Philippines");
console.log(address);

function printPlayerInfo(username, level, jobClass){

	// console.log("Username:" + username);
	// console.log("Level:" + level);
	// console.log("Job:" + jobClass);

	return "Username: " + username + "\n" + "level" + level + "\n" + "Job: " + jobClass
};

let user1 = printPlayerInfo("Clent", 91, "medium")
console.log(user1);


//returns undefined because printPlayerInfo returns nothing. It only console.logs the details. 

//You cannot save any value from printPlayerInfo() because it does not return anything.