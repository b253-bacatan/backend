// console.log("Hello World")



function addNum(num1,num2) {

	console.log(num1 + num2)
	
	
};

console.log("Displayed sum of 10 and 30")
addNum(10,30);



function subNum(num1,num2) {

	console.log(num1 - num2)
};

console.log("Displayed difference of 5 and 2")
subNum(5,2);


function multiplyNum(num1,num2) {

	let multiplication = (num1 * num2);
	return multiplication
	
};

let product = multiplyNum(5,5)
console.log("The product of 5 and 5:")
console.log(product);




function divideNum(num1,num2) {

	let division = (num1 / num2)
	return division

};

let quotient = divideNum(10,5);
console.log("The quotient of 10 and 5:")
console.log(quotient);




function getCircleArea(num1) {

	let Area = (3.14 * Math.pow(num1,2));
	return Area
};


let circleArea = getCircleArea(15)
console.log("The result of getting the area of a circle with 15 radius:")
console.log(circleArea)



function getAverage(num1, num2, num3, num4) {

	let average = (num1+num2+num3+num4)/4 
	return average
	
};


let averageVar = getAverage(1, 2, 3 ,4);
console.log("The average of 1,2,3,4");
console.log(averageVar);



function checkIfPassed(num1, num2) {

	let passed = (num1 / num2) * 100 >= 75
	return passed
};

let isPassed = checkIfPassed(38,50)
console.log("Is 38/50 a passing score")
console.log(isPassed)











// Do not modify
// For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}