// console.log("Hello World")


let array = [];

fetch("https://jsonplaceholder.typicode.com/todos/")
	.then(response => response.json())
	.then(json => array = json.map(object => object.title));
	





fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then(response => response.json())
	.then(json => console.log(json))




fetch("https://jsonplaceholder.typicode.com/todos/",{

	method: "POST",
	headers: {
		"Content-type": "application/json",
	},
	body: JSON.stringify({
		title: "Created To Do list Item",
		userID: 1,
		completed: false
	})

}).then(response => response.json())
	.then(json => console.log(json));




fetch("https://jsonplaceholder.typicode.com/todos/1", {

	method: "PUT",
	headers: {
		"Content-type": "application/json",
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		"date completed": "Pending",
		userID: 1
	})

}).then(response => response.json())
	.then(json => console.log(json));




fetch("https://jsonplaceholder.typicode.com/todos/1",{

	method: "PATCH",
	headers: {
		"Content-type": "application/json",

	},
	body: JSON.stringify({
		title: "Updated using patch"

	})

}).then(response => response.json())
	.then(json => console.log(json));