// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object



let trainer = {
	name: "Ash Ketchum",
	Age: 10,
	Pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    Friends: {
    	hoenn: ["May", "Max"],
    	kanto: ["Brock", "Misty"]
    },

	talk: function(){
		 	console.log(this.Pokemon[0] + " I choose you!");
	}

};


function Pokemon (name, level, health, attack) {
	this.name = name,
	this.level = level,
	this.health = health,
	this.attack = attack

	this.tackle = function(opponent){
		console.log(this.name + " tackled " + opponent.name)
		console.log(opponent.name + "'s health is now reduced to " + geodude.health);

	this.faint = function(life){
		console.log(opponent.name + " fainted");
	
	}	
	}
};



let pickachu = new Pokemon("Pikachu", 12, 24, 12);
let geodude = new Pokemon("Geodude", 8, 16 , 8);
let mewtwo = new Pokemon("Mewtwo", 100, 200, 100);


console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name)
console.log("Result of square bracket notation:")
console.log(trainer["Pokemon"]);
trainer.talk();
console.log(pickachu) 
console.log(geodude) 
console.log(mewtwo)
geodude.tackle(pickachu)
console.log(pickachu)
mewtwo.tackle(geodude)
console.log(geodude)










//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}