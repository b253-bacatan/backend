// console.log("Hello")


//Objective 1
// Create a function called printNumbers() that will loop over a number provided as an argument.
//In the function, add a console to display the number provided.
//In the function, create a loop that will use the number provided by the user and count down to 0
//In the loop, create an if-else statement:

// If the value provided is less than or equal to 50, terminate the loop and show the following message in the console:
//"The current value is at " + count + ". Terminating the loop."

// If the value is divisible by 10, skip printing the number and show the following message in the console:
//"The number is divisible by 10. Skipping the number."

// If the value is divisible by 5, print the number.


//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for (let i = 0; i < string.length; i++) {

	if(string[i] == 'a' || string[i] == 'e' || string[i] == 'i' || string[i] == 'o' || string[i] == 'u') {

		continue;
	
	} else {

		filteredString += string[i];

	}
}
console.log(filteredString)


//Add code here

function printNumbers(numbers) {

	console.log("The number you provide is " + numbers)

		for (let num = numbers; num >= 0; num--) {
		
		if (num <= 50) {
			
			console.log ("The current value is" + num);
			break;
			
	
		} else if (num % 10 === 0) {
			
			console.log ("The number is divisible by 10. Skipping the number");
			continue;

		} else if (num % 5 === 0) {

			console.log(num)
		}
	}

};

printNumbers(100)






	// console.log("The number you provide is " + printNumbers)







// Do not modify
// For exporting to test.js
// try {
//     module.exports = {
//        printNumbers, filteredString
//     }
// } catch(err) {

// }