const jwt = require("jsonwebtoken");


// User defined string data that will be used to create our JSON web tokens
// Used in the algorithms for encrypting our data which makes it difficult to decode the information without the defined secret keyword
const secret = "CourseBookingAPI"

// JSON web tokens

/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server


*/


// Token Creation

module.exports.createAccessToken = (user) => {

	const data = {

		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};
	// Generate a JSON web token using the jwt sign()
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
};


// Token Verification

module.exports.verify =  (req, res, next)  => {

	let token = req.headers.authorization;

	// Token received and is not undefined
	if(typeof token !== "undefined") {

		console.log(token);
		
		//Removes the "Bearer"
		token = token.slice(7, token.length);
		
		// Validate the token using the "verify" method decrypting the token using the secret code

		return jwt.verify(token, secret, (err, data) => {

			// If JWT is not valid
			if(err) {

				return res.send({ auth : "failed" });
			
			// If JWT is valid
			} else {
				// Allows the application to proceed with the next middleware function
				next();
			}

		})
	
	// Token does not exist	
	} else {

		return res.send({ auth: "failed" });
	}
};

// Token decryption

module.exports.decode = (token) => {

	// Token received and is not undefined

	if(typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err) {

				return null;
			
			} else {

				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)

				return jwt.decode(token, { complete : true }).payload
			}
		})
	
	// Token doest not exist
	} else {

		return null;
	}
};