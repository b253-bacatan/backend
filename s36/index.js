
// Setup dependencies
const express = require("express");
const mongoose =  require("mongoose");

const taskRoute = require("./routes/taskRoute")


// Server setup

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//Connection to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin123@batch253-clent.de0yplw.mongodb.net/s36?retryWrites=true&w=majority", {

	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection;

// If a connection error occurred, output in the console
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error is encountered

db.on("error", console.error.bind(console, "connection error"));


db.once("open", () => console.log(`We're now connected to the cloud database: MongoDB Atlas!`));


app.use("/tasks", taskRoute);


if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

// for testing
module.exports = app;