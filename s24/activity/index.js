// console.log("Hello World")





// Exponent Operator

const getCube = 2 ** 3


// Template Literals
console.log(`The cube of 2 is ${getCube}`);


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];


console.log(`I live at ${address[0]} ${address[1]} , ${address[2]} ${address[3]}`)


// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

console.log(`${animal.name} was a ${animal.species} . He weighed at ${animal.weight} with a measurment of ${animal.measurement}`)



// Arrow Functions

let numbers = [1, 2, 3, 4, 5];


  const myNumber = (number) => {

  	console.log(number)

};

 numbers.forEach(myNumber)




// const number1 = (acc,curr) => {
// 	return acc + curr
// }



let reduceNumber = numbers.reduce((acc,curr) => {

	return acc + curr;
});

console.log(reduceNumber);






// Javascript Classes

class dog {

	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};


const myDog = new dog("Frankie", 5 , "Miniature Dachshund")
console.log(myDog);




//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}