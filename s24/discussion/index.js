// console.log("Hello")




/* ES6 UPDATES */

// Exponent Operator

const firstNum = 8 ** 2; //ES6 update
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);


// Template literals

/*

	- Allows to write strings without using concatenation operator (+)
	-Greatly helps with code readability

*/

let name = "John";

// Pre-template literal

let message = "Hello " + name + "! Welcome";
console.log(message);


// Strings using template literals
//Uses backticks (``)
message = `Hello ${name}! Welcome to programming!`;
console.log(message)

//Multi-line using Template Literals
const anotherMessage = `${name} attended a competition with of ${firstNum}.`;

console.log(anotherMessage);


/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest is: ${principal * interestRate}`);

//Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability

		SYNTAX:
			let/const [variableName1, variableName2, variableName3] = arrayName;
*/

const fullName = ["Juan", "Dela", "Cruz"];

//Pre-array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice!`);


//Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice`);


// Object Destructuring

/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects

	SYNTAX:
		let/const {propertyName, propertyName, propertyName} = objectName;

*/


const person = {
	givenName: "Juana",
	maidenName: "Dela",
	familyName: "Cruz",
};

//Pre-object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! nice`);


// Object Destructuring
// Variable deconstructing
// const {givenName, maidenName, familyName} = person;

// console.log(givenName);
// console.log(maidenName);
// console.log(familyName);

// console.log(`Hello ${givenName} ${maidenName} ${familyName}! nice`)


/*

	Passing the values using (function) destructured properties of the object
	SYNTAX:
		function funcName ( {propertyName, propertyName, propertyName} ){
			return statement (parameters)
		}

		funcName(object)
*/

//Function Destructuring
function getFullName({ givenName, maidenName, familyName }){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);


// Arrow Functions

/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
	-  Arrow functions also have implicit return which means we don't need to use the "return" keyword.

*/

//Pre-Arrow Function
// function printFullName(firstName, lastName){
// 	console.log(`${firstName} ${lastName}`);
// }

// printFullName("JOhn", "Smith");

//Arrow Function (=>)
/*
	SYNTAX:
		let/const variableName = (parameterA, parameterB) => {
			statement
		}

		or if single line no need for curly brackets



*/

const printFullName = (firstName, lastName) => {
	console.log(`${firstName} ${lastName}`)
};

printFullName("Jane", "Smith");


const hello = () => {
	console.log("Hello");
};

hello();

const sum = (x,y) => x + y;

let total = sum(1,1);
console.log(total);


person.talk = () => "Hellooo!";

console.log(person.talk());



// Function with default Argument Value
const greet = (name = "User") => {
	return `Good morning, ${name}!`
};

console.log(greet());
console.log(greet("Ariana"));


// Class-Based Object Blueprints

/*

	Allows the creation/instantation of objects using classes as blueprints

	SYNTAX:
		class className {
			constructor (objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		};

*/

class Car {
	constructor(brand, model, year){
		this.brand = brand;
		this.model = model;
		this.year = year;
	}
}


const myCar = new Car();
console.log(myCar);

//Values of properties may be assigned after creation/instantation of an object
myCar.brand = "Ford";
myCar.model = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNew);


