
// Number 1

db.fruits.aggregate([

	{
		$match: {
			"onSale": true
		}
	},
	{
		$count: 
			"fruitsOnSale"
	},


]);


// Number 2
db.fruits.aggregate([

	{
		$match: {
			"stock": { $gte:20 }
		}
	},
	{
		$count: "enoughStock"
	}

]);


// Number 3

db.fruits.aggregate([

	{
		$match: {
			"onSale" : true
		}
	},
	{
		$group: {
			"_id": "$supplier_id", "avg_price": { $avg:"$price"}
		}
	},


]);

// Number 4

db.fruits.aggregate([

	{
		$group: {
			"_id": "$supplier_id", "max_price": { $max: "$price"}
		}
	}

]);

// Number 5


db.fruits.aggregate([

	{
		$group: {
			"_id": "$supplier_id", "min_price": { $min: "$price"}
		}
	}


]);