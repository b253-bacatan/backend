// console.log("Hello")


// JSON OBJECTS

/*
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages hence the name JavaScript Object Notation
	- Core JavaScript has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript objects
	- JavaScript objects are not to be confused with JSON
	- JSON is used for serializing different data types into bytes
	- Serialization is the process of converting data into a series of bytes for easier transmission//transfer of information
								-----JAVASCRIPT OBJECTS TO JSON STRINGS----
	- A byte is a unit of data that is eight binary digits (1 and 0) that is used to represent a character (letters, numbers or typographic symbols)
	- Bytes are information that a computer processes to perform different tasks

	 -- machine Language
	- Uses double quotes for property names
	

	  Syntax
			{
				"propertyA": "valueA",
				"propertyB": "valueB",
			}

*/

// JSON Objects

// {
// 	"city": "Quezon City",
// 	"province": "Metro Manila",
// 	"country": "Phil"
// }


// JSON Arrays

// "cities": [
//      {"city": "Quezon", "province": "mETRO MANILA", "country": "pHIL"},
//      {"city": "Quezon", "province": "mETRO MANILA", "country": "pHIL"},
//      {"city": "Quezon", "province": "mETRO MANILA", "country": "pHIL"}

// ]


// JSON METHODS
/*

/ JSON Object contains methods for parsing and converting data into a stringified JSON data format.


// Converting Data into Stringified JSON (JS Object to JSON Data Format)
	// JSON.stringify()

*/


let batchesArr = [
		{batchName: "Batch 253"},
		{batchName: "Batch 243"},


	]

console.log(batchesArr)


// The stringify method is used to convert JS objects into string/JSON Data Format
// Before sending data, it converts as array or an object to its string equivalent

console.log("Result from stringigy() method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: "Cebu",
		country: "Philippines"
	}

});

console.log(data);


// let firstName = prompt("What is your first Name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address = {
// 	city: prompt("Which city do you live in?"),
// 	country: prompt("Which country?")
// };


// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address

// });

// console.log(otherData);


// Converting stringifies JSON Data Format into JS Objects
	//Deserialization
// JSON.parse()



let batchesJSON = `[
	{
		"batchName": "Batch 253"
	},
	{
		"batchnAME": "BATCH 243"
	}
]`

console.log(batchesJSON)
// Upon receiving data, the JSON text can be converted to JS Object/s so that we can use it in our program/app.

console.log(batchesJSON);
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
	"name": "John",
	"age": 31,
	"address": {
		"province": "Bulacan"
		"country": "Phil"
	}

}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));