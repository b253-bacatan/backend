const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");



// ====================================================== Start of registering a user =====================================


router.post("/checkEmail", (req,res) => {

	// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
	// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
});



router.post("/register",  (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err))
});


// ====================================================== End of registering a user =======================================




// ====================================================== Start of Log in User =============================================


router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err));

});


// ====================================================== End of Log in User ==============================================



// ====================================== Start of Ordering a product =====================================================


router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = { 

		userId : userData.id,
		isAdmin : userData.isAdmin
	}

	if(!data.isAdmin){

		userController.addToCart(data, req.body).then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	} else {

		res.send(false);
	}

});


// ====================================== End of Ordering a product ===================================================




//======================================= Start of Retrieving user's details ===========================================


router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

		userController.getDetails(userData.id).then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));


});

//======================================= End of Retrieving user's details ===========================================



module.exports = router;




